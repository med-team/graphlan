graphlan (1.1.3-6) unstable; urgency=medium

  * Team upload.
  * Add a patch for MatPlotLib 3.8 compatibility (Closes: #1075906)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 19 Jul 2024 23:16:13 +0200

graphlan (1.1.3-5) unstable; urgency=medium

  * Drop r-cran-randomfields from Test-Depends
    Closes: #1072628
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 06 Jul 2024 22:28:02 +0200

graphlan (1.1.3-4) unstable; urgency=medium

  * Team upload.
  * d/control: update Homepage.
    Thanks to Davide Prina (Closes: #981688)
  * 2to3.patch: document existing merge request upstream
  * d/copyright: bump copyright year

 -- Étienne Mollier <emollier@debian.org>  Sun, 12 Dec 2021 10:10:28 +0100

graphlan (1.1.3-3) unstable; urgency=medium

  * Source moved to Github
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Tue, 31 Aug 2021 07:58:05 +0200

graphlan (1.1.3-2) unstable; urgency=medium

  * Use 2to3 to port to Python3
    Closes: #936652
  * debhelper-compat 12
  * Standards-Version: 4.4.1

 -- Andreas Tille <tille@debian.org>  Fri, 08 Nov 2019 17:43:16 +0100

graphlan (1.1.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Wed, 26 Sep 2018 13:35:33 +0200

graphlan (1.1-3) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Wed, 15 Nov 2017 11:52:12 +0100

graphlan (1.1-2) unstable; urgency=medium

  * Fix watch file
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jan 2017 17:13:45 +0100

graphlan (1.1-1) unstable; urgency=medium

  * Initial release (Closes: #826005)

 -- Andreas Tille <tille@debian.org>  Wed, 01 Jun 2016 13:16:34 +0200
